package com.oyuakspn.reactnative.googlefit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;

import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInOptionsExtension;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessOptions;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Device;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.data.Session;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DataReadResponse;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.Task;

import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class GoogleFitManager implements ActivityEventListener {

  private final static int REQUEST_OAUTH_REQUEST_CODE = 1;
  private final static int GOOGLE_PLAY_SERVICE_ERROR_DIALOG = 2404;

  private static final String STEPS_FIELD_NAME = "steps";
  private static final String DISTANCE_FIELD_NAME = "distance";
  private static final String HIGH_LONGITUDE = "high_longitude";
  private static final String LOW_LONGITUDE = "low_longitude";
  private static final String HIGH_LATITUDE = "high_latitude";
  private static final String LOW_LATITUDE = "low_latitude";
  private static final String HEART_RATE_AVG_BPM = "average";
  private static final String HEART_RATE_MAX_BPM = "max";
  private static final String HEART_RATE_MIN_BPM = "min";

  private Promise promise;

  @Override
  public void onNewIntent(Intent intent) { }

  public void connectGoogleFit(final Activity currentActivity, Promise promise) {
    try {
      this.promise = promise;

      FitnessOptions fitnessOptions =
        FitnessOptions.builder()
        .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
        .addDataType(DataType.TYPE_DISTANCE_DELTA, FitnessOptions.ACCESS_READ)
        .addDataType(DataType.TYPE_ACTIVITY_SEGMENT, FitnessOptions.ACCESS_READ)
        .addDataType(DataType.TYPE_HEART_RATE_BPM, FitnessOptions.ACCESS_READ)
          .build();

      if (!GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(currentActivity), fitnessOptions)) {
        GoogleSignIn.requestPermissions(
          currentActivity,
          REQUEST_OAUTH_REQUEST_CODE,
          GoogleSignIn.getLastSignedInAccount(currentActivity),
          fitnessOptions);
      } else {
        promise.resolve(true);
      }
    } catch(Exception e) {
      promise.resolve(false);
      Log.e(getClass().getName(), e.getMessage());
    }
  }

  @Override
  public void onActivityResult(Activity currentActivity, int requestCode, int resultCode, Intent data) {
    if (resultCode == Activity.RESULT_OK) {
      if (REQUEST_OAUTH_REQUEST_CODE == requestCode) {
        if (promise != null) {
          promise.resolve(true);
        }
      }
    } else {
      if (promise != null) {
        promise.resolve(false);
      }
    }
  }

  public boolean isAuthorized(final Activity currentActivity) {
    if(isGooglePlayServicesAvailable(currentActivity)) {
      FitnessOptions fitnessOptions =
        FitnessOptions.builder()
        .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
        .addDataType(DataType.TYPE_DISTANCE_DELTA, FitnessOptions.ACCESS_READ)
        .addDataType(DataType.TYPE_ACTIVITY_SEGMENT, FitnessOptions.ACCESS_READ)
        .addDataType(DataType.TYPE_HEART_RATE_BPM, FitnessOptions.ACCESS_READ)
          .build();

      return GoogleSignIn.hasPermissions(GoogleSignIn.getLastSignedInAccount(currentActivity), fitnessOptions);
    }

    return false;
  }

  public void disconnect(@NonNull Activity currentActivity, final Promise promise) {
    Fitness.getConfigClient(
      currentActivity,
      GoogleSignIn.getLastSignedInAccount(currentActivity))
      .disableFit()
      .addOnCanceledListener(new OnCanceledListener() {
        @Override
        public void onCanceled() {
          promise.resolve(false);
        }
      })
      .addOnSuccessListener(new OnSuccessListener<Void>() {
        @Override
        public void onSuccess(Void aVoid) {
          promise.resolve(true);
        }
      })
      .addOnFailureListener(new OnFailureListener() {
        @Override
        public void onFailure(@NonNull Exception e) {
          Log.e(getClass().getName(), e.getMessage());
          promise.reject(e);
        }
      });
  }

  public void revokeAccess(@NonNull Activity currentActivity, final Promise promise) {
    GoogleSignInOptionsExtension fitnessOptions =
      FitnessOptions.builder()
        .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
        .addDataType(DataType.TYPE_DISTANCE_DELTA, FitnessOptions.ACCESS_READ)
        .addDataType(DataType.TYPE_ACTIVITY_SEGMENT, FitnessOptions.ACCESS_READ)
        .addDataType(DataType.TYPE_HEART_RATE_BPM, FitnessOptions.ACCESS_READ)
        .build();
    GoogleSignInOptions signInOptions = new GoogleSignInOptions.Builder().addExtension(fitnessOptions).build();
    GoogleSignIn.getClient(currentActivity, signInOptions)
        .revokeAccess()
        .addOnCanceledListener(new OnCanceledListener() {
        @Override
        public void onCanceled() {
          promise.resolve(false);
        }
        })
        .addOnSuccessListener(new OnSuccessListener<Void>() {
        @Override
        public void onSuccess(Void aVoid) {
          promise.resolve(true);
        }
        })
        .addOnFailureListener(new OnFailureListener() {
        @Override
        public void onFailure(@NonNull Exception e) {
          Log.e(getClass().getName(), e.getMessage());
          promise.reject(e);
        }
        })
        .addOnFailureListener(new OnFailureListener() {
        @Override
        public void onFailure(@NonNull Exception e) {
          Log.e(getClass().getName(), e.getMessage());
          promise.reject(e);
        }
    });
  }

  public void getSteps(Activity currentActivity, double startDate, double endDate, int intervalAmount,  String intervalUnit, final Promise promise){
    TimeUnit timeInterval = getInterval(intervalUnit);
    WritableArray steps = Arguments.createArray();

    DataSource datasource = new DataSource.Builder()
      .setAppPackageName("com.google.android.gms")
      .setDataType(DataType.TYPE_STEP_COUNT_DELTA)
      .setType(DataSource.TYPE_DERIVED)
      .setStreamName("estimated_steps")
      .build();

    DataReadRequest readRequest = new DataReadRequest.Builder()
      .aggregate(datasource)
      .bucketByTime(intervalAmount, timeInterval)
      .setTimeRange((long) startDate, (long) endDate, TimeUnit.MILLISECONDS)
      .build();

    Fitness.getHistoryClient(currentActivity, getSignedInAccountForExtension(currentActivity))
      .readData(readRequest)
      .addOnSuccessListener(new OnSuccessListener<DataReadResponse>() {
        @Override
        public void onSuccess(DataReadResponse dataReadResponse) {
          if (dataReadResponse.getBuckets().size() > 0) {
            for (Bucket bucket : dataReadResponse.getBuckets()) {
              List<DataSet> dataSets = bucket.getDataSets();
              for (DataSet dataSet : dataSets) {
                processStep(dataSet, steps);
              }
            }
          }

          promise.resolve(steps);
        }
      })
      .addOnFailureListener(new OnFailureListener() {
        @Override
        public void onFailure(@NonNull Exception e) {
          promise.reject(e);
        }
      })
      .addOnCompleteListener(new OnCompleteListener<DataReadResponse>() {
        @Override
        public void onComplete(@NonNull Task<DataReadResponse> task) {
        }
      });
  }

  public void getActivities(Activity currentActivity, double startDate, double endDate, int intervalAmount, String intervalUnit, final Promise promise) {
    TimeUnit timeInterval = getInterval(intervalUnit);
    WritableArray results = Arguments.createArray();

    DataReadRequest readRequest = new DataReadRequest.Builder()
      .aggregate(DataType.TYPE_STEP_COUNT_DELTA, DataType.AGGREGATE_STEP_COUNT_DELTA)
      .aggregate(DataType.TYPE_DISTANCE_DELTA, DataType.AGGREGATE_DISTANCE_DELTA)
      .aggregate(DataType.TYPE_HEART_RATE_BPM, DataType.AGGREGATE_HEART_RATE_SUMMARY)
      .bucketBySession(intervalAmount, timeInterval)
      .setTimeRange((long) startDate, (long) endDate, TimeUnit.MILLISECONDS)
      .build();

      Fitness.getHistoryClient(currentActivity, getSignedInAccountForExtension(currentActivity))
        .readData(readRequest)
        .addOnSuccessListener(new OnSuccessListener<DataReadResponse>() {
          @Override
          public void onSuccess(DataReadResponse dataReadResponse) {
            if (dataReadResponse.getBuckets().size() > 0) {
              List<Bucket> buckets = dataReadResponse.getBuckets();

              for (Bucket bucket : buckets) {
                processActivity(currentActivity, bucket, results);
              }
            }

            promise.resolve(results);
          }
        })
        .addOnFailureListener(new OnFailureListener() {
          @Override
          public void onFailure(@NonNull Exception e) {
            promise.reject(e);
          }
        })
        .addOnCompleteListener(new OnCompleteListener<DataReadResponse>() {
          @Override
          public void onComplete(@NonNull Task<DataReadResponse> task) {
          }
        });
  }

  public void getHeartRates(Activity currentActivity, double startDate, double endDate, final Promise promise) {
    WritableArray heartRates = Arguments.createArray();
    DataReadRequest readRequest = new DataReadRequest.Builder()
      .aggregate(DataType.TYPE_HEART_RATE_BPM, DataType.AGGREGATE_HEART_RATE_SUMMARY)
      .bucketByTime(1, TimeUnit.MINUTES)
      .setTimeRange((long) startDate, (long) endDate, TimeUnit.MILLISECONDS)
      .build();

    Fitness.getHistoryClient(currentActivity, getSignedInAccountForExtension(currentActivity))
      .readData(readRequest)
      .addOnSuccessListener(new OnSuccessListener<DataReadResponse>() {
        @Override
        public void onSuccess(DataReadResponse dataReadResponse) {
          if (dataReadResponse.getBuckets().size() > 0) {
            for (Bucket bucket : dataReadResponse.getBuckets()) {
              List<DataSet> dataSets = bucket.getDataSets();
              for (DataSet dataSet : dataSets) {
                if (!dataSet.isEmpty()) {
                  processHeartRate(dataSet, heartRates);
                }
              }
            }
          }

          promise.resolve(heartRates);
        }
      })
      .addOnFailureListener(new OnFailureListener() {
        @Override
        public void onFailure(@NonNull Exception e) {
          promise.reject(e);
        }
      })
      .addOnCompleteListener(new OnCompleteListener<DataReadResponse>() {
        @Override
        public void onComplete(@NonNull Task<DataReadResponse> task) {
        }
      });
  }

  private static TimeUnit getInterval(String intervalUnit) {
    if(intervalUnit.equals("minute")) {
      return TimeUnit.MINUTES;
    }
    if(intervalUnit.equals("hour")) {
      return TimeUnit.HOURS;
    }
    return TimeUnit.DAYS;
  }

  private static boolean isGooglePlayServicesAvailable(final Activity currentActivity) {
    GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();

    int status = googleApiAvailability.isGooglePlayServicesAvailable(currentActivity);
    if (status != ConnectionResult.SUCCESS) {
      if (googleApiAvailability.isUserResolvableError(status)) {
        googleApiAvailability.getErrorDialog(currentActivity, status, GOOGLE_PLAY_SERVICE_ERROR_DIALOG).show();
      }
      return false;
    }

    return true;
  }

  private static GoogleSignInAccount getSignedInAccountForExtension(final Activity currentActivity) {
    GoogleSignInOptionsExtension fitnessOptions =
      FitnessOptions.builder()
        .addDataType(DataType.TYPE_STEP_COUNT_DELTA, FitnessOptions.ACCESS_READ)
        .addDataType(DataType.TYPE_DISTANCE_DELTA, FitnessOptions.ACCESS_READ)
        .addDataType(DataType.TYPE_CALORIES_EXPENDED, FitnessOptions.ACCESS_READ)
        .addDataType(DataType.TYPE_ACTIVITY_SEGMENT, FitnessOptions.ACCESS_READ)
        .addDataType(DataType.TYPE_HEART_RATE_BPM, FitnessOptions.ACCESS_READ)
        .build();
    GoogleSignInAccount account = GoogleSignIn.getAccountForExtension(currentActivity, fitnessOptions);
    return account;
  }

  private void processStep(DataSet dataSet, WritableArray map) {

    WritableMap stepMap = Arguments.createMap();

    for (DataPoint dp : dataSet.getDataPoints()) {

      WritableMap source = Arguments.createMap();
      if (dp.getDataSource().getAppPackageName() != null) {
        source.putString("appPackage", dp.getDataSource().getAppPackageName());
      } else {
        source.putNull("appPackage");
      }

      Device sourceDevice = dp.getDataSource().getDevice();
      getDeviceSourceDetail(sourceDevice, source);


      WritableMap originalSource = Arguments.createMap();
      if (dp.getOriginalDataSource().getAppPackageName() != null) {
        originalSource.putString("appPackage", dp.getOriginalDataSource().getAppPackageName());
      } else {
        originalSource.putNull("appPackage");
      }

      Device originalSourceDevice = dp.getOriginalDataSource().getDevice();
      getDeviceSourceDetail(originalSourceDevice, originalSource);

      for(Field field : dp.getDataType().getFields()) {
        stepMap.putMap("source", source);
        stepMap.putMap("originalSource", originalSource);
        stepMap.putDouble("start", dp.getStartTime(TimeUnit.MILLISECONDS));
        stepMap.putDouble("end", dp.getEndTime(TimeUnit.MILLISECONDS));
        stepMap.putDouble("steps", dp.getValue(field).asInt());

        map.pushMap(stepMap);
      }
    }
  }

  private void processActivity(Activity currentActivity, Bucket bucket, WritableArray results) {
    if (!bucket.getDataSets().isEmpty()) {
      Session session = bucket.getSession();
      String activityName = session.getActivity();

      long start = session.getStartTime(TimeUnit.MILLISECONDS);
      long end = session.getEndTime(TimeUnit.MILLISECONDS);
      Date startDate = new Date(start);
      Date endDate = new Date(end);

      WritableMap map = Arguments.createMap();
      map.putDouble("start", start);
      map.putDouble("end", end);
      map.putString("activityName", activityName);

      boolean isTracked = true;

      for (DataSet dataSet : bucket.getDataSets()) {
        for (DataPoint dataPoint : dataSet.getDataPoints()) {

          WritableMap source = Arguments.createMap();
          if (dataPoint.getDataSource().getAppPackageName() != null) {
            source.putString("appPackage", dataPoint.getDataSource().getAppPackageName());
          } else {
            source.putNull("appPackage");
          }

          Device sourceDevice = dataPoint.getDataSource().getDevice();
          getDeviceSourceDetail(sourceDevice, source);


          WritableMap originalSource = Arguments.createMap();
          if (dataPoint.getOriginalDataSource().getAppPackageName() != null) {
            originalSource.putString("appPackage", dataPoint.getOriginalDataSource().getAppPackageName());
          } else {
            originalSource.putNull("appPackage");
          }

          Device originalSourceDevice = dataPoint.getOriginalDataSource().getDevice();
          getDeviceSourceDetail(originalSourceDevice, originalSource);

          if (startDate.getTime() % 1000 == 0 && endDate.getTime() % 1000 == 0) {
            isTracked = false;
          }

          map.putMap("source", source);
          map.putMap("originalSource", originalSource);
          map.putBoolean("tracked", isTracked);

          for (Field field : dataPoint.getDataType().getFields()) {
            String fieldName = field.getName();
            switch (fieldName) {
              case STEPS_FIELD_NAME:
                map.putDouble("step", dataPoint.getValue(field).asInt());
                break;
              case DISTANCE_FIELD_NAME:
                map.putDouble(fieldName, dataPoint.getValue(field).asFloat());
                break;
              case HEART_RATE_AVG_BPM:
                map.putDouble("averageBpm", dataPoint.getValue(field).asFloat());
                break;
              case HEART_RATE_MAX_BPM:
                map.putDouble("maxBpm", dataPoint.getValue(field).asFloat());
                break;
              case HEART_RATE_MIN_BPM:
                map.putDouble("minBpm", dataPoint.getValue(field).asFloat());
                break;
              default:
                Log.w("sourceName", "don't specified and handled: " + fieldName);
            }
          }
        }
      }

      results.pushMap(map);
    }
  }

  private void processHeartRate(DataSet dataSet, WritableArray map) {

    WritableMap heartRateMap = Arguments.createMap();
    for (DataPoint dp : dataSet.getDataPoints()) {
      heartRateMap.putDouble("start", dp.getStartTime(TimeUnit.MILLISECONDS));
      heartRateMap.putDouble("end", dp.getEndTime(TimeUnit.MILLISECONDS));

      for(Field field : dp.getDataType().getFields()) {
        String fieldName = field.getName();
        heartRateMap.putDouble(fieldName, dp.getValue(field).asFloat());
      }
    }

    map.pushMap(heartRateMap);
  }

  private void getDeviceSourceDetail(Device sourceDevice, WritableMap source) {
    if (sourceDevice != null) {
      source.putString("deviceManufacturer", sourceDevice.getManufacturer());
      source.putString("deviceModel", sourceDevice.getModel());
      source.putInt("deviceType", sourceDevice.getType());
      switch(sourceDevice.getType()) {
        case Device.TYPE_CHEST_STRAP:
          source.putString("deviceTypeName", "Chest Strap"); break;
        case Device.TYPE_HEAD_MOUNTED:
          source.putString("deviceTypeName", "Head-mounted"); break;
        case Device.TYPE_PHONE:
          source.putString("deviceTypeName", "Android Phone"); break;
        case Device.TYPE_TABLET:
          source.putString("deviceTypeName", "Android Tablet"); break;
        case Device.TYPE_SCALE:
          source.putString("deviceTypeName", "Scale"); break;
        case Device.TYPE_WATCH:
          source.putString("deviceTypeName", "Android Wear"); break;
        case Device.TYPE_UNKNOWN:
          source.putString("deviceTypeName", "Unknown"); break;
      }
    } else {
      source.putNull("deviceManufacturer");
      source.putNull("deviceModel");
      source.putNull("deviceType");
      source.putNull("deviceTypeName");
    }
  }

}
