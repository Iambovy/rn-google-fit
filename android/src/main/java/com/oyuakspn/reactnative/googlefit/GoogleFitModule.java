// All methods to react
package com.oyuakspn.reactnative.googlefit;

import android.app.Activity;
import android.util.Log;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Promise;

public class GoogleFitModule extends ReactContextBaseJavaModule{
  private static final String REACT_MODULE = "RNGoogleFit";
  private final static String TAG = GoogleFitModule.class.getName();

  private final GoogleFitManager mGoogleFitManager;

  public GoogleFitModule(ReactApplicationContext reactContext) {
    super(reactContext);

    this.mGoogleFitManager = new GoogleFitManager();
    reactContext.addActivityEventListener(this.mGoogleFitManager);
  }

  @Override
  public String getName() {
    return REACT_MODULE;
  }

  @ReactMethod
  public void connectGoogleFit(Promise promise){
    final Activity activity = getCurrentActivity();

    if(activity != null) {
      this.mGoogleFitManager.connectGoogleFit(activity, promise);
    } else {
      promise.reject(new Throwable());
    }
  }

  @ReactMethod
  public void isAuthorized(Promise promise){
    promise.resolve(this.mGoogleFitManager.isAuthorized(getCurrentActivity()));
  }

  @ReactMethod
  public void disconnect(Promise promise){
    final Activity activity = getCurrentActivity();
    if(activity != null) {
      this.mGoogleFitManager.disconnect(activity, promise);
    } else {
      promise.reject(new Throwable());
    }
  }

  @ReactMethod
  public void revokeAccess(Promise promise){
    final Activity activity = getCurrentActivity();
    if(activity != null) {
      this.mGoogleFitManager.revokeAccess(activity, promise);
    } else {
      promise.reject(new Throwable());
    }
  }

  @ReactMethod
  public void getActivities(double startDate, double endDate, int intervalAmount, String intervalUnit, Promise promise){
    try {
      this.mGoogleFitManager.getActivities(getCurrentActivity(), startDate, endDate, intervalAmount, intervalUnit, promise);
    }catch(Error e){
      promise.reject(e);
    }
  }

  @ReactMethod
  public void getSteps(double startDate, double endDate, int intervalAmount, String intervalUnit, Promise promise){
    try {
      this.mGoogleFitManager.getSteps(getCurrentActivity(), startDate, endDate, intervalAmount, intervalUnit, promise);
    }catch(Error e){
      promise.reject(e);
    }
  }

  @ReactMethod
  public void getHeartRates(double startDate, double endDate, Promise promise){
    try {
      this.mGoogleFitManager.getHeartRates(getCurrentActivity(), startDate, endDate, promise);
    }catch(Error e){
      promise.reject(e);
    }
  }

}
