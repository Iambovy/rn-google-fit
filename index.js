import { NativeModules } from 'react-native'

/**
 * Connect to GoogleFit
 */
const connectGoogleFit = async () => {
  const result = await NativeModules.RNGoogleFit.connectGoogleFit()
  return result
}

/**
 * Get user steps count by time interval and datetime range
 * @param startDate (long)
 * @param endDate (long)
 * @param intervalAmount (integer)
 * @param intervalUnit (string) [minute, hour, day]
 * @returns {*}
 */
const getSteps = async ({
  startDate,
  endDate,
  intervalAmount = 1,
  intervalUnit = 'day',
}) => {
  const result = await NativeModules.RNGoogleFit.getSteps(
    startDate,
    endDate,
    intervalAmount,
    intervalUnit
  )
  return result
}

/**
 * Get user activities by time interval and datetime range
 * @param startDate (long)
 * @param endDate (long)
 * @param intervalAmount (integer)
 * @param intervalUnit (string) [minute, hour, day]
 * @returns {*}
 */
const getActivities = async ({
  startDate,
  endDate,
  intervalAmount = 1,
  intervalUnit = 'day',
}) => {
  const result = await NativeModules.RNGoogleFit.getActivities(
    startDate,
    endDate,
    intervalAmount,
    intervalUnit
  )
  return result
}

/**
 * Get user heart rates by datetime range
 * @param startDate (long)
 * @param endDate (long)
 * @returns {*}
 */
const getHeartRates = async ({ startDate, endDate }) => {
  const result = await NativeModules.RNGoogleFit.getHeartRates(
    startDate,
    endDate
  )
  return result
}

/**
 * Disconnect from Google Fit
 */
const disconnectGoogleFit = async () => {
  const result = await NativeModules.RNGoogleFit.disconnect()
  return result
}

const revokeAccess = async () => {
  const result = await NativeModules.RNGoogleFit.revokeAccess()
  return result
}

/**
 * Check if the user has already authorized the app
 */
const isAuthorized = async () => {
  const result = await NativeModules.RNGoogleFit.isAuthorized()
  return result
}

export default {
  connectGoogleFit,
  getActivities,
  getHeartRates,
  getSteps,
  disconnectGoogleFit,
  revokeAccess,
  isAuthorized,
}
