# rn-google-fit

A React Native module for interacting with Google Fit Platform

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
